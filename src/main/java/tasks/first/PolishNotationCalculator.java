package tasks.first;

public class PolishNotationCalculator {

    public Long reverseCalculator(String s) throws IllegalArgumentException {
        String[] chars = s.split(" ");
        int numIndex = chars.length / 2;
        int operationIndex = numIndex + 1;
        long num1;
        try {
            num1 = Long.parseLong(chars[numIndex]);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("You argument contains not number value");
        }
        long num2;
        while (numIndex > 0 && operationIndex < chars.length) {
            try {
                num2 = Long.parseLong(chars[--numIndex]);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("You argument contains not number value");
            }
            if (num1 < 0 || num2 < 0) {
                throw new IllegalArgumentException("Illegal number was found in argument");
            }
            switch (chars[operationIndex]) {
                case "+":
                    num1 += num2;
                    break;
                case "-":
                    num1 -= num2;
                    break;
                case "*":
                    num1 *= num2;
                    break;
                case "/":
                    num1 /= num2;
                    break;
                default:
                    throw new IllegalArgumentException("Unknown arithmetic operation");
            }
            operationIndex++;
        }
        if (numIndex != 0 || operationIndex != chars.length) {
            throw new IllegalArgumentException("Your argument isn't correct(not enough numbers or operations), please, check it and rewrite in a correct form");
        }
        return num1;
    }
}
