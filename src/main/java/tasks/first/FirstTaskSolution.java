package tasks.first;

public class FirstTaskSolution implements FirstTask {
    @Override
    public String breadthFirst(boolean[][] adjacencyMatrix, int startIndex) {
        return new BreadthSearch().breadthFirst(adjacencyMatrix, startIndex);

    }

    @Override
    public Boolean validateBrackets(String s) {
        return new CheckBracket().checkBrackets(s);
    }

    @Override
    public Long polishCalculation(String s) {
        return new PolishNotationCalculator().reverseCalculator(s);
    }
}
