package tasks.first;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;
import java.util.Scanner;

public class BreadthSearch {
    private final Scanner scanner = new Scanner(System.in);
    private Vertex[] vertexes;


    private boolean[][] getAdjacencyMatrixFromUser() {
        System.out.println("Сколько вершин в вашем графе?");
        int countOfVertexes = Integer.parseInt(scanner.nextLine());
        System.out.println("Введите матрицу смежности графа");
        boolean[][] adjacencyMatrix = new boolean[countOfVertexes][countOfVertexes];
        for (int i = 0; i < countOfVertexes; i++) {
            for (int j = 0; j < countOfVertexes; j++) {
                adjacencyMatrix[i][j] = Integer.parseInt(scanner.next()) == 1;
            }
        }
        scanner.nextLine();
        return adjacencyMatrix;
    }

    private void createGraph(boolean[][] adjacencyMatrix) {
        vertexes = new Vertex[adjacencyMatrix.length];
        for (int i = 0; i < vertexes.length; i++) {
            vertexes[i] = new Vertex(i + 1);
        }
        ArrayList<Vertex> descendants = new ArrayList<>();
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            descendants.clear();
            for (int j = 0; j < adjacencyMatrix.length; j++) {
                if (adjacencyMatrix[i][j]) {
                    descendants.add(vertexes[j]);
                }
            }
            vertexes[i].setDescendants(descendants);
        }
    }

    public String breadthFirst(boolean[][] adjacencyMatrix, int indexStar) {
        createGraph(adjacencyMatrix);
        StringBuilder ans = new StringBuilder();
        Queue<Vertex> queueOfVertexes = new ArrayDeque<>(adjacencyMatrix.length);
        queueOfVertexes.add(vertexes[indexStar - 1]);
        vertexes[indexStar - 1].setTaken(true);
        ans.append(indexStar).append(",");
        Vertex parent;
        while (queueOfVertexes.peek() != null) {
            parent = queueOfVertexes.poll();
            for (Vertex descendant : parent.getDescendants()) {
                if (!descendant.isTaken()) {
                    queueOfVertexes.offer(descendant);
                    descendant.setTaken(true);
                    ans.append(descendant.getNumber()).append(",");
                }
            }
        }
        ans.deleteCharAt(ans.length() - 1);
        for (Vertex vertex : vertexes) {
            vertex.setTaken(false);
        }
        return ans.toString();
    }
}

