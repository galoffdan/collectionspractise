package tasks.first;

import java.util.ArrayDeque;

public class CheckBracket {
    private final String openingBrackets = "({[";
    private final String closingBrackets = ")}]";


    public boolean checkBrackets(String s) {
        ArrayDeque<String> brackets = new ArrayDeque<>();
        String ch;
        for (int i = 0; i < s.length(); i++) {
            ch = s.substring(i, i + 1);
            if (openingBrackets.contains(ch)) {
                brackets.push(ch);
                continue;
            }
            if (closingBrackets.contains(ch) && openingBrackets.indexOf(brackets.pop()) != closingBrackets.indexOf(ch)) {
                return false;
            }
        }
        return true;
    }
}
