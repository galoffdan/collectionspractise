package tasks.first;

import java.util.ArrayList;

public class Vertex {
    private final int number;
    private ArrayList<Vertex> descendants;
    private boolean isTaken = false;

    public Vertex(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public ArrayList<Vertex> getDescendants() {
        return descendants;
    }

    public void setDescendants(ArrayList<Vertex> descendants) {
        this.descendants = new ArrayList<>(descendants);
    }

    public boolean isTaken() {
        return isTaken;
    }

    public void setTaken(boolean taken) {
        isTaken = taken;
    }
}
